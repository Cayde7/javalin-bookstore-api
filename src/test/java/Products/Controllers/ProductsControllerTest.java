package Products.Controllers;

import Products.Models.ProductList;
import io.javalin.http.Context;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ProductsControllerTest {

    private final Context context = mock(Context.class);

    @Test
    void testGetProducts() throws Exception {
        ProductList productList = new ProductList();
        productList.initializeContent();
        ProductsController productsController = new ProductsController(productList);
        productsController.getProducts.handle(context);
        verify(context).json(productList.getProducts());
    }

    @Test
    public void testAddProduct() {
    }

    @Test
    public void testBuyProduct() {
    }

    @Test
    public void testTestGetProducts() {
    }
}