package ShoppingCart.Controllers;

import API.APIController;
import Products.Exceptions.ProductNotFoundException;
import Products.Models.Product;
import Products.Exceptions.StockEmptyException;
import Products.Models.ProductList;
import ShoppingCart.Models.ShoppingCart;
import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;

import static API.API.returnError;

public class ShoppingCartController implements APIController {

    private ShoppingCart cart;

    private ProductList productList;

    public ShoppingCartController(ShoppingCart cart, ProductList productList) {
        this.cart = cart;
        this.productList = productList;
    }

    @Override
    public void addHandlers(Javalin app) {
        // get all products in cart
        app.get("/cart", getCart);

        // TODO: PUT, DELETE, POST
    }


    protected final Handler getCart = (@NotNull Context context) -> context.json(cart);

    protected final Handler addProduct = (@NotNull Context context) -> {
        int id = Integer.parseInt(context.pathParam("productId"));
        try {
            Product product = productList.findById(id);
            context.status(201);
            ShoppingCart updatedCart = cart.addToCart(product);
            context.json(updatedCart);
        } catch (StockEmptyException e) {
            // product is not in stock, return error
            returnError(context, 405, e);
        } catch (ProductNotFoundException e) {
            // product is not found, return error
            returnError(context, 404, e);
        }
    };


    protected final Handler removeProduct = (@NotNull Context context) -> {
        int id = Integer.parseInt(context.pathParam("productId"));
        try {
            Product product = productList.findById(id);
            context.json(cart.removeFromCart(product));
        } catch (Exception e) {
            // product is not found, return error
            returnError(context, 404, e);
        }
    };

    protected final Handler clearCart = (@NotNull Context context) -> {
        cart.clear();
        context.json(cart);
    };

}
