package ShoppingCart.Models;

import Products.Exceptions.StockEmptyException;
import Products.Models.Product;
import com.fasterxml.jackson.annotation.JsonGetter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShoppingCart {

    private final ArrayList<OrderItem> items = new ArrayList<>();

    public List<OrderItem> getItems() {
        return Collections.unmodifiableList(items);
    }

    public void addItem(OrderItem item) {
        this.items.add(item);
    }

    public void removeItem(OrderItem item) {
        this.items.remove(item);
    }

    public void clear() {
        this.items.clear();
    }

    @JsonGetter("total")
    public float totalPrice() {
        float total = 0;
        for (OrderItem item : items) {
            total += item.getProduct().getPrice() * item.getQuantity();
        }
        return total;
    }

    public ShoppingCart addToCart(Product product) throws StockEmptyException {

        if (!product.isInStock()) {
            throw new StockEmptyException();
        }

        // if order is already in cart, increase quantity
        for (OrderItem order : getItems()) {
            if (order.getProduct() == product) {
                order.incrementQuantity();
                product.decrementStock();
                return this;
            }
        }

        // otherwise, add order to cart
        OrderItem order = new OrderItem(product, 1);
        addItem(order);
        product.decrementStock();
        return this;
    }

    public ShoppingCart removeFromCart(Product product) throws Exception {
        for (OrderItem order : getItems()) {
            if (order.getProduct() == product) {
                if (order.getQuantity() > 1) {
                    order.decrementQuantity();
                } else {
                    removeItem(order);
                }
                product.incrementStock();

                return this;
            }
        }
        throw new Exception("Product not in cart");
    }

}
