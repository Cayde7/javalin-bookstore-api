package ShoppingCart.Models;

import Products.Models.Product;

public class OrderItem {
    private final Product product;
    private int quantity;

    public OrderItem(Product product, int amount) {
        this.product = product;
        this.quantity = amount;
    }

    public int getQuantity() {
        return quantity;
    }

    public void incrementQuantity() {
        this.quantity++;
    }

    public void decrementQuantity() {
        this.quantity--;
    }

    public Product getProduct() {
        return product;
    }
}
