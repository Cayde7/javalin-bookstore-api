package Customers.Models;

import ShoppingCart.Models.ShoppingCart;

public class Customer {
    private final String name;
    private final ShoppingCart cart = new ShoppingCart();

    // using a singleton here since we don't have a login / session yet
    private static Customer singleInstance = new Customer("Jan de Vries");

    public static Customer instance() {
        return singleInstance;
    }

    private Customer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ShoppingCart getShoppingCart() {
        return cart;
    }
}
