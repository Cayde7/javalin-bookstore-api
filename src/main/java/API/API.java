package API;

import Products.Models.ProductList;
import Customers.Models.Customer;
import ShoppingCart.Controllers.ShoppingCartController;

import Products.Controllers.ProductsController;

import ShoppingCart.Models.ShoppingCart;
import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.plugin.bundled.CorsPluginConfig;

public class API {
    private final Javalin app;

    private final ProductsController productsController;
    private final ShoppingCartController cartController;

    public API() {

        // start webserver
        int port = 7070;
        app = Javalin.create(config ->
                config.plugins.enableCors(cors -> cors.add(CorsPluginConfig::anyHost))).start(port);

        ProductList productList = new ProductList();
        productList.initializeContent();

        this.productsController = new ProductsController(productList);

        ShoppingCart shoppingCart = Customer.instance().getShoppingCart();
        this.cartController = new ShoppingCartController(shoppingCart, productList);

        addHandlers();
    }

    private void addHandlers() {
        app.get("/test", ctx -> ctx.result("test"));

        productsController.addHandlers(app);
        cartController.addHandlers(app);
    }

    public static void returnError(Context ctx, int status, Exception exception) {
        APIError error = new APIError(404, exception.getMessage());
        ctx.status(status);
        ctx.json(error);
    }
}
