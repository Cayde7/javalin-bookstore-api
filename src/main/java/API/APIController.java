package API;

import io.javalin.Javalin;

public interface APIController {

    void addHandlers(Javalin app);
}
