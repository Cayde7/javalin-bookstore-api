package Products.Exceptions;

public class StockEmptyException extends Exception {
    public StockEmptyException() {
        super("Stock is empty!");
    }
}
