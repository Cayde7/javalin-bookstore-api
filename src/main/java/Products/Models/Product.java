package Products.Models;

import Products.Exceptions.StockEmptyException;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/*
    These annotations make sure product JSON is deserialized
    to the appropriate object based on the 'type'-property.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Book.class, name = "book"),
        @JsonSubTypes.Type(value = DVD.class, name = "dvd")
})
public abstract class Product {
    protected Integer id;

    protected String title;

    protected Float price;
    protected Integer stock = 1;

    protected Product() {
    }

    protected Product(int id, String title, float price) {
        this.id = id;
        this.title = title;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isInStock() {
        return stock > 0;
    }

    public void incrementStock() {
        stock += 1;
    }

    public void decrementStock() throws StockEmptyException {
        if (stock > 0) {
            stock -= 1;
        } else {
            throw new StockEmptyException();
        }
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void update(Product product) {
        this.price = product.price;
    }
}
