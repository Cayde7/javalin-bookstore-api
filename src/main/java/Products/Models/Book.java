package Products.Models;

public class Book extends Product {
    private Author author;

    // empty default constructor is needed for JSON deserialization
    public Book() {
        super();
    }

    public Book(int id, String title, Author author, float price) {
        super(id, title, price);
        this.author = author;
    }

    public Author getAuthor() {
        return author;
    }

    @Override
    public void update(Product product) {
        if (product instanceof Book book) {
            super.update(book);
            this.title = book.title;
            this.author = book.author;
        } else {
            throw new UnsupportedOperationException("Can only update instances of type Book");
        }
    }
}

