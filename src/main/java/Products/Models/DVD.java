package Products.Models;

public class DVD extends Product {

    private Integer durationMinutes;

    // empty default constructor is needed for JSON deserialization

    public DVD() {}
    public DVD(int id, String title, float price, int durationMinutes) {
        super(id, title, price);
        this.durationMinutes = durationMinutes;
    }

    public Integer getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(Integer durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    @Override
    public void update(Product product) {
        if (product instanceof DVD dvd) {
            super.update(product);
            this.durationMinutes = dvd.durationMinutes;
        }
    }
}
