package Products.Models;

public class Author {
    private String name;

    // empty default constructor is needed for JSON deserialization
    public Author() {}

    public Author(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
