package Products.Models;

import Products.Exceptions.ProductNotFoundException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProductList {

    private final List<Product> products = new ArrayList<>();

    public List<Product> getProducts() {
        return Collections.unmodifiableList(products);
    }

    public void add(Product product) {
        products.add(product);
    }

    public void initializeContent() {
        // create some products
        Author author = new Author("J. K. Rowling");
        Book harryPotter = new Book(0, "Harry Potter and the Philosopher's Stone", author, 10);
        products.add(harryPotter);
        Book harryPotter2 = new Book(1, "Harry Potter and the Chamber of Secrets", author, 12);
        harryPotter2.setStock(3);

        products.add(harryPotter2);
        DVD dieHard = new DVD(2, "Die Hard with a Vengeance", 14, 123);
        dieHard.setStock(5);
        products.add(dieHard);
    }
    public Product findById(int id) throws ProductNotFoundException {
        for (Product product : products) {
            if (product.getId() == id) {
                return product;
            }
        }
        throw new ProductNotFoundException();
    }

    public void remove(Product product) {
        products.remove(product);
    }

    public void update(int id, Product product) throws ProductNotFoundException {
        Product productToUpdate = findById(id);
        productToUpdate.update(product);
    }
}
