package Products.Controllers;

import API.APIController;
import Products.Exceptions.ProductNotFoundException;
import Products.Models.Product;
import Products.Models.ProductList;
import io.javalin.Javalin;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.jetbrains.annotations.NotNull;

import static API.API.returnError;

public class ProductsController implements APIController {

    private ProductList productList;

    public ProductsController(ProductList productList) {
        this.productList = productList;
    }

    @Override
    public void addHandlers(Javalin app) {
        // get all products
        app.get("/products", getProducts);

        // get specific product by id
        app.get("/products/{id}", getProductById);

        // TODO: POST, DELETE, PUT
    }

    protected final Handler getProducts = (@NotNull Context context) ->
            context.json(productList.getProducts());

    protected final Handler getProductById = (@NotNull Context context) -> {
        int id = Integer.parseInt(context.pathParam("id"));
        try {
            Product product = productList.findById(id);
            context.json(product);
        } catch (ProductNotFoundException e) {
            // product is not found, return error
            returnError(context, 404, e);
        }
    };

    // TODO: this should be handled by database
    private int getNextId() {
        int max = 0;
        for (Product product : productList.getProducts()) {
            int id = product.getId();
            if (id >= max) {
                max = id;
            }
        }
        return max + 1;
    }
}
