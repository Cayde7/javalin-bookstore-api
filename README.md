# Javalin Bookstore API

Een voorbeeld van een backend-applicatie met REST API gebouwd met Java en [Javalin](https://www.javalin.io).

De applicatie heeft twee hoofdfunctionaliteiten:
1. Het tonen van een lijst met producten en het bijhouden van hun voorraad.
2. Het beheren van een winkelmandje, waar producten aan toegevoegd en uit verwijderd kunnen worden.

## Klassendiagram

![class diagram](class-diagram.png)
